﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LoLSDK;

public class EndSceneController : MonoBehaviour {
	public string[] EndSceneVN;
	public TMPro.TextMeshProUGUI ThankYouLabel;
	public string ThankYouJSON;

	// Use this for initialization
	IEnumerator Start () {
		EventManager.Instance.TriggerEvent(new VNEvent(EndSceneVN, true));
		SimpleJSON.JSONNode defs = SharedState.LanguageDefs;
		ThankYouLabel.text = defs[ThankYouJSON];

		yield return new WaitForSeconds(5f);
		LoLSDK.LOLSDK.Instance.StopSound("BGM/bensound-funkyelement.mp3");
		LOLSDK.Instance.CompleteGame();
	}
}
