﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnergyDot : MonoBehaviour {
	public Vector3 position;
	public Tween DotTween;
	private SpriteRenderer sprite;

	void Awake() {
		position = transform.position;
		sprite = GetComponent<SpriteRenderer>();
	}

	public void StartTween() {
		DotTween = transform.DOPunchScale(new Vector3(1.3f, 1.3f, 1.3f), 1.5f, 5, 1).SetLoops(-1);
		DotTween.Play();
	}

	public void StopTween() {
		DotTween.Kill();
		transform.DOScale(new Vector3(3, 3, 3), 0.25f);
	}

	public void Shine() {
		sprite.color = Color.white;
	}
}
