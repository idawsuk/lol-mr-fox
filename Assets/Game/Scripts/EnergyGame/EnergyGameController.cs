﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnergyGameController : MonoBehaviour {
	private LineRenderer lineRenderer;
	[SerializeField]
	private GameObject[] DotSet;
	[SerializeField]
	private EnergyDot[] Dots;
	private int SetProgress = 0;
	private int Progress = 0;
	private Vector2 mousePosition;
	private Camera mainCamera;
	[SerializeField]
	private LayerMask ItemLayerName;

	private GameObject firstObject, secondObject;
	private ProgressionManager progression;

	[Header("Animation")]
	public string InteractAnimationName;
	public float InteractDuration;
	private string IdleAnimation;
	private Spine.Unity.SkeletonAnimation skeletonAnimation;

	[Header("Dialogue")]
	public bool WaitVNBeforeProgressing = false;
	public string[] OpeningVN, SuccessVN, FinishingVN;
	public int SuccessVNProgress;

	[Header("Cinematic Ending")]
	public string CinematicAnimation;
	public string[] CinematicTexts;
	public SpriteRenderer Overlay;

	// Use this for initialization
	void Start () {
		mainCamera = Camera.main;
		lineRenderer = GetComponent<LineRenderer>();
		progression = FindObjectOfType<ProgressionManager>();
		progression.VNSystem.IsCinematic = false;
		skeletonAnimation = GetComponentInChildren<Spine.Unity.SkeletonAnimation>();
		IdleAnimation = skeletonAnimation.AnimationName;

		Dots = DotSet[0].GetComponentsInChildren<EnergyDot>();
		lineRenderer.SetPosition(0, Dots[0].position);
		lineRenderer.SetPosition(1, Dots[0].position);

		Dots[0].StartTween();
		Dots[0].Shine();
		Dots[1].StartTween();

		EventManager.Instance.TriggerEvent(new VNEvent(OpeningVN));

		if(WaitVNBeforeProgressing) {
			EventManager.Instance.AddListener<VNFinishEvent>(NextLevel);
		}
	}

	void OnDestroy()
	{
		if(EventManager.Instance != null) {
			EventManager.Instance.RemoveListener<VNFinishEvent>(NextLevel);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Progress + 1 >= Dots.Length || VNSystem.VNActive)
			return;

		if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WebGLPlayer) {
			mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);

			Collider2D hitCollider = Physics2D.OverlapPoint(mousePosition, ItemLayerName);

			if(Input.GetMouseButtonDown(0)) {
				if(hitCollider != null) {
					if(CheckObject(hitCollider.gameObject, Progress)) {
						firstObject = hitCollider.gameObject;
						lineRenderer.SetPosition(Progress, firstObject.transform.position);
					}
				} else {
					firstObject = null;
				}
			}

			if(Input.GetMouseButton(0)) {
				if(hitCollider != null && firstObject != null) {
					if(CheckObject(hitCollider.gameObject, Progress + 1)) {
						lineRenderer.SetPosition(Progress + 1, Dots[Progress + 1].transform.position);
						secondObject = hitCollider.gameObject;
					} else if(firstObject != null) {
						lineRenderer.SetPosition(Progress + 1, mousePosition);
						secondObject = null;
					}
				} else if(firstObject != null) {
					lineRenderer.SetPosition(Progress + 1, mousePosition);
					secondObject = null;
				}
			}

			if(Input.GetMouseButtonUp(0)) {
				if(firstObject != null && secondObject != null) {
					Dots[Progress].StopTween();
					lineRenderer.positionCount++;
					Progress++;
					Dots[Progress].Shine();
					lineRenderer.SetPosition(Progress + 1, Dots[Progress].position);
					firstObject = null;
					secondObject = null;
					LoLSDK.LOLSDK.Instance.PlaySound("SFX/pick.wav");


					if(Progress >= Dots.Length - 1) {
						LoadNextSet();
					} else {
						Dots[Progress + 1].StartTween();
					}
				} else {
					lineRenderer.SetPosition(Progress + 1, Dots[Progress].position);
					firstObject = null;
					secondObject = null;
				}
			}
		}
	}

	bool CheckObject(GameObject obj, int index) {
		return obj == Dots[index].gameObject;
	}

	IEnumerator PlayInteractAnimation() {
		skeletonAnimation.AnimationName = InteractAnimationName;

		yield return new WaitForSeconds(InteractDuration);

		skeletonAnimation.AnimationName = IdleAnimation;
	}

	void LoadNextSet() {
		SetProgress++;

		if(SetProgress == SuccessVNProgress)
			EventManager.Instance.TriggerEvent(new VNEvent(SuccessVN));

		if(SetProgress < DotSet.Length) {
			DotSet[SetProgress - 1].SetActive(false);
			DotSet[SetProgress].SetActive(true);

			Dots = DotSet[SetProgress].GetComponentsInChildren<EnergyDot>();

			Vector3[] pos = new Vector3[2];
			pos[0] = Dots[0].position;
			pos[1] = Dots[0].position;
			lineRenderer.positionCount = 2;
			lineRenderer.SetPositions(pos);
			Progress = 0;

			Dots[0].StartTween();
			Dots[0].Shine();
			Dots[1].StartTween();

			StopCoroutine(PlayInteractAnimation());
			StartCoroutine(PlayInteractAnimation());

			LoLSDK.LOLSDK.Instance.PlaySound("SFX/great.wav");
		} else {
			EventManager.Instance.TriggerEvent(new VNEvent(FinishingVN, true));

			if(!WaitVNBeforeProgressing) {
				if(!string.IsNullOrEmpty(CinematicAnimation)) {
					Overlay.DOColor(Color.white, 1f).OnComplete(StartCinematic);
					EventManager.Instance.AddListener<VNFinishEvent>(NextLevel);
				} else
					progression.PlayNextGame();
			}
		}
	}

	void NextLevel(VNFinishEvent e) {
		// progression.PlayNextGame();

		if(string.IsNullOrEmpty(CinematicAnimation)) {
			progression.PlayNextGame();
		} else {
			Overlay.DOColor(Color.white, 1f).OnComplete(StartCinematic);
		}
	}

	void StartCinematic() {
		skeletonAnimation.AnimationName = CinematicAnimation;
		CinematicAnimation = "";
		Overlay.DOColor(Color.clear, 1f).OnComplete(CinematicVN);
	}

	void CinematicVN() {
		EventManager.Instance.TriggerEvent(new VNEvent(CinematicTexts, true));
	}
}
