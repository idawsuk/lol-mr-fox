﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class ProgressionManager : MonoBehaviour {
	public GameObject[] GameList;

	[SerializeField]
	private int CurrentGameIndex;
	private GameObject CurrentGame;
	public Image ScreenFader;

	public bool DebugMode;

	public VNSystem VNSystem;

	// Use this for initialization
	void Awake () {
		VNSystem = FindObjectOfType<VNSystem>();

		if(!DebugMode)
			LoadGame(CurrentGameIndex);

		ScreenFader.DOColor(Color.clear, 1f);
	}

	// Update is called once per frame
	void Update () {

	}

	public void PlayNextGame() {
		FadeOutScreen();
	}

	public void LoadGame(int index) {
		GameObject obj = Instantiate(GameList[index]) as GameObject;
		obj.name = obj.name.Replace("(Clone)", "");
		CurrentGame = obj;
	}

	void FadeInScreen() {
		NextGame();
		ScreenFader.DOColor(Color.clear, 1f).OnComplete(delegate {
			ScreenFader.raycastTarget = false;
		});
	}

	void FadeOutScreen() {
		ScreenFader.raycastTarget = true;
		ScreenFader.DOColor(Color.black, 1f).OnComplete(FadeInScreen);
	}

	void NextGame() {
		Destroy(CurrentGame);
		CurrentGame = null;
		CurrentGameIndex++;
		LoLSDK.LOLSDK.Instance.SubmitProgress(0, CurrentGameIndex, GameList.Length);

		if(CurrentGameIndex < GameList.Length) {
			LoadGame(CurrentGameIndex);
		} else {
			UnityEngine.SceneManagement.SceneManager.LoadScene("FinishScene");
		}
	}
}
