﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine.Unity;

public class DogoHuntMode : MonoBehaviour {
	private FlickControl control;
	[SerializeField]
	private GameObject Shadow;
	[SerializeField]
	private GameObject[] Bandages;
	private int Hurt;
	private SkeletonAnimation skeletonAnimation;
	private string CurrentAnimation;
	private HuntGameManager gameManager;
	public string FoxType = "Fox";

	// Use this for initialization
	void Start () {
		gameManager = FindObjectOfType<HuntGameManager>();
		control = GetComponent<FlickControl>();
		skeletonAnimation = GetComponentInChildren<SkeletonAnimation>();
		CurrentAnimation = skeletonAnimation.AnimationName;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D coll) {
		//food
		if(coll.tag == "Food") {
			Destroy(coll.gameObject);
			NomNom();
		}

		//obstacle
		if(coll.tag == "Obstacle") {
			StartCoroutine(HurtDogo());
		}

		if(coll.tag == "Boundary") {
			Fall();
		}
	}

	void NomNom() {
		LoLSDK.LOLSDK.Instance.PlaySound("SFX/great.wav");
		gameManager.Progression();
	}

	IEnumerator HurtDogo() {
		EventManager.Instance.TriggerEvent(new CameraShakeEvent(1f, 0.25f));
		LoLSDK.LOLSDK.Instance.PlaySound("SFX/put.wav");
		skeletonAnimation.AnimationName = FoxType + "-hit-obstacle";
		control.StopMovement();

		yield return new WaitForSeconds(0.25f);

		if(Hurt < Bandages.Length) {
			// Bandages[Hurt].SetActive(true);
			Hurt++;
			skeletonAnimation.AnimationName = "Fox-idle-patch"+Hurt;
			CurrentAnimation = FoxType + "-idle-patch"+Hurt;
		} else {
			skeletonAnimation.AnimationName = CurrentAnimation;
		}
	}

	void Fall() {
		Shadow.SetActive(false);
		transform.DORotate(new Vector3(0, 0, transform.localScale.x * 89f), 1f).SetEase(Ease.Linear);
		transform.DOScale(new Vector3(transform.localScale.x * 0.3f, 0.3f, 0.3f), 1f).SetEase(Ease.Linear).OnComplete(delegate {
			Respawn();
			HurtDogo();
		});
	}

	void Respawn() {
		Shadow.SetActive(true);
		transform.eulerAngles = Vector3.zero;
		transform.localScale = Vector3.one;
		transform.position = control.StartPosition;
	}
}
