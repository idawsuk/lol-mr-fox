﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour {
	public string AnimationToPlay;
	public float AnimationTime;
	private string initAnimation;
	private Spine.Unity.SkeletonAnimation skelAnimation;

	void Start()
	{
		skelAnimation = GetComponent<Spine.Unity.SkeletonAnimation>();
		initAnimation = skelAnimation.AnimationName;
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Food" || other.tag == "Player") {
			StartCoroutine(PlayAnimation());
		}
	}

	IEnumerator PlayAnimation() {
		skelAnimation.AnimationName = AnimationToPlay;

		yield return new WaitForSeconds(AnimationTime);

		skelAnimation.AnimationName = initAnimation;
	}


}
