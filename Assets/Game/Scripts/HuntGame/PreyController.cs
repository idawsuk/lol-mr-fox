﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PreyController : MonoBehaviour {
	public List<Vector3> PatrolPoints;
	[SerializeField]
	private float DurationMin = 0.5f, DurationMax = 3f;

	private Tween MovingTween;
	private int CurrentPoint;
	private float scaleX;
	[SerializeField]
	private float MovementSpeed = 5f;

	// Use this for initialization
	IEnumerator Start () {
		yield return new WaitForSeconds(0.3f);
		StartMove();
		scaleX = transform.localScale.x;
	}

	// Update is called once per frame
	void Update () {

	}

	IEnumerator Move() {
		yield return new WaitForSeconds(Random.Range(DurationMin, DurationMax));
		int randomIndex = Random.Range(0, PatrolPoints.Count);
		while(randomIndex == CurrentPoint && PatrolPoints.Count > 1) {
			randomIndex = Random.Range(0, PatrolPoints.Count);
		}

		CurrentPoint = randomIndex;
		if(transform.localPosition.x < PatrolPoints[CurrentPoint].x) {
			transform.localScale = new Vector3(-scaleX, transform.localScale.y, transform.localScale.z);
		} else {
			transform.localScale = new Vector3(scaleX, transform.localScale.y, transform.localScale.z);
		}

		float time = Vector3.Distance(transform.localPosition, PatrolPoints[CurrentPoint]) / MovementSpeed;

		MovingTween = transform.DOLocalMove(PatrolPoints[CurrentPoint], time).SetEase(Ease.InOutCubic).OnComplete(StartMove);
	}

	void StartMove() {
		StartCoroutine(Move());
	}

	public void Eaten() {
		MovingTween.Kill();
	}
}
