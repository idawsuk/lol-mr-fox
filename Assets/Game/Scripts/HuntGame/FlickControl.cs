﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickControl : MonoBehaviour {
	private Vector2 mousePosition;
	private Camera mainCamera;
	[SerializeField]
	private LayerMask ItemLayerName;
	private GameObject touchObject;

	private LineRenderer lineRenderer;
	private Rigidbody2D rigidbody;
	[SerializeField]
	[Range(0f, 1f)]
	private float Friction = 0.2f;
	[SerializeField]
	private float Speed = 100f;
	private Vector2 velocity = Vector2.zero;
	public Vector3 StartPosition;

	// Use this for initialization
	void Start () {
		mainCamera = Camera.main;
		lineRenderer = GetComponentInChildren<LineRenderer>();
		lineRenderer.gameObject.SetActive(false);
		rigidbody = GetComponent<Rigidbody2D>();
		StartPosition = transform.position;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rigidbody.velocity = rigidbody.velocity * (1f - Friction);
		if(VNSystem.VNActive)
			return;
			
		if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WebGLPlayer) {
			mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);

			if(touchObject == null) {
				Collider2D hitCollider = Physics2D.OverlapPoint(mousePosition, ItemLayerName);

				if(Input.GetMouseButtonDown(0)) {
					if (hitCollider != null)
					{
						touchObject = hitCollider.gameObject;

						lineRenderer.gameObject.SetActive(true);

						lineRenderer.SetPosition(0, transform.position);
						lineRenderer.SetPosition(1, mousePosition);
					}
					else
					{
						lineRenderer.gameObject.SetActive(false);
						touchObject = null;
					}
				}

				if(Input.GetMouseButtonUp(0)) {
					if(touchObject == null)
						lineRenderer.gameObject.SetActive(false);
				}
			} else {
				if(Input.GetMouseButton(0)) {

					lineRenderer.SetPosition(0, transform.position);
					lineRenderer.SetPosition(1, mousePosition);

					if(((Vector2)transform.position - mousePosition).x > 0) {
						transform.localScale = new Vector3(-1, 1, 1);
					}
					if(((Vector2)transform.position - mousePosition).x < 0) {
						transform.localScale = new Vector3(1, 1, 1);
					}
				}

				if(Input.GetMouseButtonUp(0)) {
					touchObject = null;
					lineRenderer.gameObject.SetActive(false);

					rigidbody.AddForce(((Vector2)transform.position - mousePosition) * Speed);
				}
			}
		}
		transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.y);
	}

	public void StopMovement() {
		rigidbody.velocity = Vector2.zero;
	}
}
