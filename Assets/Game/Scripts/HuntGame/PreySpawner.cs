﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PreySpawner : MonoBehaviour {
	public GameObject[] PreyPrefab;
	public Transform parent;

	// Use this for initialization
	void Start () {

	}

	public void Spawn() {
		StartCoroutine(IESpawn());
	}

	IEnumerator IESpawn() {
		yield return new WaitForSeconds(0.2f);

		int randomIndex = Random.Range(0, PreyPrefab.Length);
		PreyController controller = PreyPrefab[randomIndex].GetComponent<PreyController>();
		Vector3 spawnPos = controller.PatrolPoints[Random.Range(0, controller.PatrolPoints.Count)];

		GameObject preyObj = Instantiate(PreyPrefab[randomIndex]) as GameObject;
		preyObj.transform.SetParent(parent);
		preyObj.transform.localPosition = spawnPos;

		Vector3 initScale = preyObj.transform.localScale;
		preyObj.transform.localScale = Vector3.zero;

		preyObj.SetActive(true);
		preyObj.transform.DOScale(initScale, 0.25f).SetEase(Ease.OutBack);
	}
}
