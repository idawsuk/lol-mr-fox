﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HuntGameManager : MonoBehaviour {
	public int GatheredFood = 0;
	public int FoodTarget = 5;
	private ProgressionManager progression;
	public string[] OpeningVN, SuccessVN, FinishingVN;
	public int SuccessVNProgress;
	public bool WaitVNBeforeProgressing = false;
	public PreySpawner spawner;

	// Use this for initialization
	void Start () {
		progression = FindObjectOfType<ProgressionManager>();
		progression.VNSystem.IsCinematic = false;
		EventManager.Instance.TriggerEvent(new VNEvent(OpeningVN));
		spawner = FindObjectOfType<PreySpawner>();
		spawner.Spawn();

		if(WaitVNBeforeProgressing) {
			EventManager.Instance.AddListener<VNFinishEvent>(NextLevel);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnDestroy()
	{
		if(EventManager.Instance != null) {
			EventManager.Instance.RemoveListener<VNFinishEvent>(NextLevel);
		}
	}

	public void Progression() {
		GatheredFood++;
		if(GatheredFood >= FoodTarget) {
			EventManager.Instance.TriggerEvent(new VNEvent(FinishingVN, true));

			if(!WaitVNBeforeProgressing)
				progression.PlayNextGame();
		} else {
			spawner.Spawn();
		}

		if(GatheredFood == SuccessVNProgress) {
			EventManager.Instance.TriggerEvent(new VNEvent(SuccessVN));
		}
	}

	void NextLevel(VNFinishEvent e) {
		progression.PlayNextGame();
	}
}
