﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeEvent : GameEvent {
	public float Amount;
	public float Duration;

	public CameraShakeEvent(float amount, float duration) {
		Amount = amount;
		Duration = duration;
	}
}
