﻿public class VNEvent : GameEvent {
	public string[] Messages;
	public bool LastVNBeforeNextLevel = false;

	public VNEvent(string[] messages) {
		Messages = messages;
	}

	public VNEvent(string[] messages, bool last) {
		Messages = messages;
		LastVNBeforeNextLevel = last;
	}
}

public class VNFinishEvent : GameEvent {

}
