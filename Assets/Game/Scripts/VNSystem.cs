﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class VNSystem : MonoBehaviour {
	public TMPro.TextMeshProUGUI MessageText;
	public GameObject VNUI;
	private string[] messages;
	private int currentMessage;
	public static bool VNActive;
	private bool LastVN = false;
	public bool IsCinematic = false;
	private bool DelayInput = false;
	private bool Processing = false;
	public GameObject[] Tutorials;
	public TMPro.TextMeshProUGUI TapToContinueText;

	private JSONNode defs;

	// Use this for initialization
	void Awake () {
		VNSystem.VNActive = false;
		EventManager.Instance.AddListener<VNEvent>(OnVNEvent);
		defs = SharedState.LanguageDefs;

		if(TapToContinueText != null)
			TapToContinueText.text = defs["dialogue-continue"];
	}

	void OnDestroy()
	{
		if(EventManager.Instance != null)
			EventManager.Instance.RemoveListener<VNEvent>(OnVNEvent);
	}

	// Update is called once per frame
	void Update () {
		if(VNSystem.VNActive && !IsCinematic && !DelayInput) {
			if(Input.GetMouseButtonDown(0)) {
				if(!Processing) {
					currentMessage++;
					if(currentMessage < messages.Length) {
						StartCoroutine(ActivateVN(defs[messages[currentMessage]]));
					} else {
						if(LastVN)
							EventManager.Instance.TriggerEvent(new VNFinishEvent());
						DisableVN();
					}
				} else {
					LoLSDK.LOLSDK.Instance.StopSound("SFX/text.wav");
					StopCoroutine(ActivateVN(defs[messages[currentMessage]]));
					Processing = false;
					MessageText.maxVisibleCharacters = defs[messages[currentMessage]].ToString().Length;
				}
			}
		}
	}

	void OnVNEvent(VNEvent e) {
		if(e.Messages.Length > 0) {
			if(TapToContinueText != null)
				TapToContinueText.gameObject.SetActive(!IsCinematic);
				
			// Time.timeScale = 0;
			LastVN = e.LastVNBeforeNextLevel;
			messages = e.Messages;
			VNUI.SetActive(true);
			currentMessage = 0;

			StartCoroutine(ActivateVN(defs[messages[currentMessage]]));
		}
	}

	IEnumerator ShowText(string message) {
		LoLSDK.LOLSDK.Instance.SpeakText(messages[currentMessage]);
		for (int i = 0; i < Tutorials.Length; i++)
		{
			if(messages[currentMessage] == Tutorials[i].name) {
				Tutorials[i].SetActive(true);
			} else {
				Tutorials[i].SetActive(false);
			}
		}
		MessageText.text = message;

		int totalVisibleCharacter = message.Length;
		int counter = 0;

		Processing = true;

		LoLSDK.LOLSDK.Instance.PlaySound("SFX/text.wav", false, true);

		while(Processing) {
			int visibleCount = counter % (totalVisibleCharacter + 1);

			MessageText.maxVisibleCharacters = visibleCount;

			if(visibleCount >= totalVisibleCharacter)
				Processing = false;

			counter++;

			yield return null;
		}

		LoLSDK.LOLSDK.Instance.StopSound("SFX/text.wav");
	}

	IEnumerator ActivateVN(string message) {
		DelayInput = true;
		StartCoroutine(ShowText(message));

		yield return new WaitForSeconds(0.5f);
		VNSystem.VNActive = true;

		yield return new WaitForSeconds(0.5f);
		DelayInput = false;
	}

	void DisableVN() {
		VNSystem.VNActive = false;
		VNUI.SetActive(false);

		LoLSDK.LOLSDK.Instance.StopSound("SFX/text.wav");
	}

	public void HideCinematicVN(AnimationEvent e) {
		VNSystem.VNActive = false;
		VNUI.SetActive(false);
	}
}
