﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine.Unity;

public class EatGameController : MonoBehaviour {
	public Fox.DragAndDrop dragAndDropController;
	private FoodSpawner Spawner;
	private Food food;

	[SerializeField]
	private int munchTime = 5;
	private int currentMunch = 0;
	private ProgressionManager progression;

	public string IdleAnimation;
	public string ChewAnimation;
	public float ChewDuration;
	public string WrongAnimation;
	public float WrongDuration;
	private string CurrentAnimation;
	private SkeletonAnimation skeletonAnimation;
	private bool IsIdle = true;

	public string[] OpeningVN, SuccessVN, FinishingVN;
	public int SuccessVNProgress;
	public bool WaitVNBeforeProgressing = false;
	// Use this for initialization
	void Start () {
		dragAndDropController = FindObjectOfType<Fox.DragAndDrop>();
		Spawner = FindObjectOfType<FoodSpawner>();
		Spawner.SpawnFood();
		progression = FindObjectOfType<ProgressionManager>();
		progression.VNSystem.IsCinematic = false;
		skeletonAnimation = GetComponentInChildren<SkeletonAnimation>();

		EventManager.Instance.TriggerEvent(new VNEvent(OpeningVN));
		if(WaitVNBeforeProgressing) {
			EventManager.Instance.AddListener<VNFinishEvent>(NextLevel);
		}
	}

	void OnDestroy()
	{
		if(EventManager.Instance != null) {
			EventManager.Instance.RemoveListener<VNFinishEvent>(NextLevel);
		}
	}

	public void EatFood(Food food) {
		// update progress -> spawn food
		// tap2 eat
		this.food = food;
		this.food.collider2D.enabled = false;
		currentMunch = 0;
		dragAndDropController.enabled = false;
	}

    public void OnMouseDown()
    {
		if(VNSystem.VNActive)
			return;

		if(food != null && IsIdle) {
			if(food.Type == Food.FoodType.FOOD) {
				food.transform.DOShakePosition(0.25f, 0.25f, 20, 20, false, true);
				currentMunch++;

				StartCoroutine(PlayAnimation(ChewAnimation, ChewDuration));

				if(currentMunch >= munchTime) {
					CheckSpawn();

					food.GetComponent<DraggableItem>().Eat();

					LoLSDK.LOLSDK.Instance.PlaySound("SFX/great.wav");

					food = null;

					dragAndDropController.enabled = true;
				} else {
					LoLSDK.LOLSDK.Instance.PlaySound("SFX/put.wav");
				}
			} else {
				LoLSDK.LOLSDK.Instance.PlaySound("SFX/dizzy.wav");
				PlayWrongAnimation();
				food.GetComponent<DraggableItem>().ResetPosition();
				dragAndDropController.enabled = true;

				food = null;

			}
		}
    }

	public void CheckSpawn() {
		if(Spawner.SpawnFood()) {
			if(Spawner.CurrentFoodIndex == SuccessVNProgress) {
				EventManager.Instance.TriggerEvent(new VNEvent(SuccessVN));
			}
		} else {
			EventManager.Instance.TriggerEvent(new VNEvent(FinishingVN, true));

			if(!WaitVNBeforeProgressing)
				progression.PlayNextGame();
		}
	}

	public void PlayWrongAnimation() {
		StartCoroutine(PlayAnimation(WrongAnimation, WrongDuration));
	}

	IEnumerator PlayAnimation(string name, float duration) {
		skeletonAnimation.AnimationName = name;
		IsIdle = false;

		yield return new WaitForSeconds(duration);

		skeletonAnimation.AnimationName = IdleAnimation;
		IsIdle = true;
	}

	void NextLevel(VNFinishEvent e) {
		progression.PlayNextGame();
	}
}
