﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DraggableItem : MonoBehaviour {
	private EatGameController controller;
	private FoodSpawner foodSpawner;
	private Food Food;
	private TrashCan TrashCan;


	// Use this for initialization
	void Start () {
		foodSpawner = FindObjectOfType<FoodSpawner>();
		Food = GetComponent<Food>();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "GameController")
			controller = other.gameObject.GetComponent<EatGameController>();

		if(other.tag == "Trash") {
			TrashCan = other.gameObject.GetComponent<TrashCan>();
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if(other.tag == "GameController")
			controller = null;

		if(other.tag == "Trash") {
			TrashCan = null;
		}
	}

	public void Release() {
		if(controller != null) {
			// Eat();
			controller.EatFood(Food);
			LoLSDK.LOLSDK.Instance.PlaySound("SFX/poeh.wav");
		} else if(TrashCan != null) {
			TrashCan.PutAway(Food);
		} else {
			ResetPosition();
		}
	}

	public void StopTween() {
		foodSpawner.SpawnTween.Kill();
	}

	public void Eat() {
		Destroy(gameObject);
	}

	public void ResetPosition() {
		controller = null;
		TrashCan = null;
		transform.DOMove(foodSpawner.SpawnPosition, 0.5f).SetEase(Ease.OutQuad).OnComplete(EnableDragAndDrop);

	}

	void EnableDragAndDrop() {
		Food.collider2D.enabled = true;
		controller.dragAndDropController.enabled = true;
	}
}
