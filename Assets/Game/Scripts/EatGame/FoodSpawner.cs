﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FoodSpawner : MonoBehaviour {
	public Vector3 SpawnPosition;

	public GameObject[] ItemSpawn;

	public int CurrentFoodIndex = 0;
	public Tween SpawnTween;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public bool SpawnFood() {
		if(CurrentFoodIndex >= ItemSpawn.Length) {
			return false;
		} else {
			SpawnTween = ItemSpawn[CurrentFoodIndex].transform.DOMove(SpawnPosition, 2f).SetEase(Ease.OutQuad);
			CurrentFoodIndex++;
			return true;
		}
	}
}
