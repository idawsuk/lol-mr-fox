﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class TrashCan : MonoBehaviour {
	private EatGameController controller;
	public SkeletonAnimation Skeleton;
	private string IdleAnimation;
	public string InteractAnimation;
	public float InteractDuration;

	// Use this for initialization
	void Start () {
		controller = FindObjectOfType<EatGameController>();
		IdleAnimation = Skeleton.AnimationName;
	}

	public void PutAway(Food food) {
		DraggableItem draggable = food.GetComponent<DraggableItem>();
		if(food.Type == Food.FoodType.NON_FOOD) {
			draggable.Eat();
			controller.CheckSpawn();
		} else {
			draggable.ResetPosition();
			controller.PlayWrongAnimation();
		}

		StartCoroutine(IEInteractAnimation());
	}

	IEnumerator IEInteractAnimation() {
		Skeleton.AnimationName = InteractAnimation;

		yield return new WaitForSeconds(InteractDuration);

		Skeleton.AnimationName = IdleAnimation;
	}
}
