﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fox
{
	public class DragAndDrop : MonoBehaviour {
		public LayerMask ItemLayer;
		public string TargetTag;
		private Vector3 mousePosition;
		private Camera mainCamera;
		private GameObject touchObject;

		// Use this for initialization
		void Start () {
			mainCamera = Camera.main;
		}

		// Update is called once per frame
		void Update () {
			if(VNSystem.VNActive)
				return;
				
			if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WebGLPlayer) {
				mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
				mousePosition.z = 0;

				Collider2D hitCollider = Physics2D.OverlapPoint(mousePosition, ItemLayer);

				if(Input.GetMouseButtonDown(0)) {
					if (hitCollider != null)
					{
						touchObject = hitCollider.gameObject;
						DraggableItem item = touchObject.GetComponent<DraggableItem>();
						if(item != null) {
							item.StopTween();
						}
					}
					else
					{
						touchObject = null;
					}
				}

				if(Input.GetMouseButton(0)) {
					if(touchObject != null) {
						touchObject.transform.position = mousePosition;
					}
				}

				if(Input.GetMouseButtonUp(0)) {
					if(touchObject != null) {
						DraggableItem item = touchObject.GetComponent<DraggableItem>();
						if(item != null) {
							item.Release();
						}
						touchObject = null;
					}
				}
			}	
		}
	}
}
