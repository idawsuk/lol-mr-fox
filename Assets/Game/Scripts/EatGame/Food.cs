﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour {
	public enum FoodType {
		FOOD, NON_FOOD
	}

	public FoodType Type;
	public BoxCollider2D collider2D;

	void Start() {
		collider2D = GetComponent<BoxCollider2D>();
	}
}
