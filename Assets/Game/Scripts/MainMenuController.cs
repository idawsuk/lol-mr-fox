﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour {
	public TMPro.TextMeshProUGUI TapText;

	// Use this for initialization
	void Start () {
		LoLSDK.LOLSDK.Instance.PlaySound("BGM/bensound-funkyelement.mp3", true, true);
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)) {
			UnityEngine.SceneManagement.SceneManager.LoadScene("GameScene");
		}
	}
}
