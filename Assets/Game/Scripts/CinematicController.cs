﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class CinematicController : MonoBehaviour {
	public ProgressionManager progression;
	public SkeletonAnimation[] SpineObjects;

	// Use this for initialization
	void Start () {
		progression = FindObjectOfType<ProgressionManager>();
		progression.VNSystem.IsCinematic = true;
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void SpineAnimationEventHandler(AnimationEvent e) {
		SpineObjects[e.intParameter].AnimationName = e.stringParameter;
	}

	public void NextScene(AnimationEvent e) {
		progression.PlayNextGame();
	}

	public void ShowVN(AnimationEvent e) {
		string[] messages = new string[1];
		messages[0] = e.stringParameter;
		EventManager.Instance.TriggerEvent(new VNEvent(messages));
	}

	public void CameraShake(AnimationEvent e) {
		EventManager.Instance.TriggerEvent(new CameraShakeEvent(1f, 0.25f));
	}
}
